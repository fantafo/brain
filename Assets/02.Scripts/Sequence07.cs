﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sequence07 : MonoBehaviour
{
    [SerializeField]
    private GameObject m_SevenSequence;   // 7.시퀀스 오브젝트
    [SerializeField]
    private GameObject m_CameraContainer;   // 7.시퀀스 카메라 컨테이너

    [SerializeField]
    private GameObject m_ParticleElec01;
    [SerializeField]
    private GameObject m_ParticleElec02;
    

    public GameObject sevenSequence
    {
        get { return m_SevenSequence; }
    }

    // 7.시퀀스 시작 함수
    public void StartSequence7()
    {
        m_SevenSequence.SetActive(true);  // 7.시퀀스 켜기

        // 메인카메라 컨테이너 붙이기 + 초기화
        GameManager.Instance.InitMainCamera(m_CameraContainer);

        // 트랙킹 초기화 
        GameManager.Instance.TrackingRecenter();

        // 6.씨퀀스 끄기
        GetComponent<Sequence06>().sixSequence.SetActive(false);

        StartCoroutine(NarrationOnSQ7());
    }

    IEnumerator NarrationOnSQ7()
    {
        yield return new WaitForSeconds(1f);
        GameManager.Instance.NarrationOn(18);
        yield return new WaitForSeconds(10f);
        GameManager.Instance.NarrationOn(19);
        yield return null;
    }

    public void DuringPath()
    {
        m_ParticleElec01.SetActive(false);

    }

    public void SQ7PathFinish()
    {
        StartCoroutine(LoadSequence8());
    }

    // 8.시퀀스 호출
    IEnumerator LoadSequence8()
    {
        yield return null;

        GameManager.Instance.OnChangeSequence -= GetComponent<Sequence07>().StartSequence7;
        GameManager.Instance.OnChangeSequence += GetComponent<Sequence08>().StartSequence8;

        GameManager.Instance.FadeOut(true);     // Fade out 후 Fade in 자동 실행
    }
}
