﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTF;
using FTF.Packet;

public class InterationItemSQ3 : InteractionItem
{   
    
    // 인터렉션 실행 함수
    public override void HandleSelectionComplete()
    {
        if (base.m_GazeOver && PlayerInstance.main.IsRoomMaster)
        {
            base.m_SelectionRadial.Hide(); // Active Radial 숨기기

            if (gameObject.name.Contains("Recticle"))
            {
                Networker.Send(C_Brain.Index(5));
                GameObject.FindObjectOfType<Sequence03>().ZoomIntoFinger();
            }           

            gameObject.SetActive(false);
        }
    }
}
