﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sequence04 : MonoBehaviour
{
    [SerializeField]
    private GameObject m_FourSequence;   // 4.시퀀스 오브젝트
    [SerializeField]
    private GameObject m_CameraContainer;   // 4.시퀀스 카메라 컨테이너

    public GameObject fourSequence
    {
        get { return m_FourSequence; }
    }

    // 4.시퀀스 시작 함수
    public void StartSequence4()
    {
        m_FourSequence.SetActive(true);  // 4.시퀀스 켜기

        // 메인카메라 컨테이너 붙이기 + 초기화
        GameManager.Instance.InitMainCamera(m_CameraContainer);

        // 트랙킹 초기화 
        GameManager.Instance.TrackingRecenter();

        // 3.씨퀀스 끄기
        GetComponent<Sequence03>().threeSequence.SetActive(false);

        StartCoroutine(NarrationOnSQ4());
    }

    IEnumerator NarrationOnSQ4()
    {
        GameManager.Instance.BGMOn(2);
        GameManager.Instance.bgmSound.volume = 0.15f;
        GameManager.Instance.NarrationOn(13);    // 나레이션_13번
        yield return null;
    }

    public void SQ4PathFinish()
    {
        StartCoroutine(LoadSequence5());
    }

    // 5.시퀀스 호출
    IEnumerator LoadSequence5()
    {
        yield return new WaitForSeconds(GameManager.Instance.sequenceDuration);     // 대기

        GameManager.Instance.OnChangeSequence -= GetComponent<Sequence04>().StartSequence4;
        GameManager.Instance.OnChangeSequence += GetComponent<Sequence05>().StartSequence5;

        GameManager.Instance.FadeOut(true);     // Fade out 후 Fade in 자동 실행
    }
}
