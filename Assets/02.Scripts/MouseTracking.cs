﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 마우스 카메라 트랙킹 클래스
public class MouseTracking : MonoBehaviour
{
    // 카메라 
    private Camera m_camera = null;

    // 마우스 현재 vector 값
    private float rotationX = 0f;
    private float rotationY = 0f;

    // 마우스 X, Y 민감도
    private float sensitivityX = 5.0f;
    private float sensitivityY = 5.0f;

    // 마우스 Y축 각도 제한
    private float minimumY = -70.0f;
    private float maximumY = 70.0f;

    void Awake()
    {
        m_camera = transform.GetComponent<Camera>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0)) // 왼쪽 마우스 클릭 : 마우스 rotaion값 초기화
        {
            // 씨퀀스 이동에 따라 카메라가 초기화 됐다면, rotaion 값도 초기화할 것.
            // 그래야 다음 씨퀀스에서 화면이 튀지 않는다.
            if (m_camera.transform.localEulerAngles == Vector3.zero)
            {
                rotationX = 0f;
                rotationY = 0f;
            }
        }

        if (Input.GetMouseButtonDown(1)) // 오른쪽 마우스 클릭 : 시야 초기화.
        {
            m_camera.transform.localEulerAngles = Vector3.zero;
        }

        if (Input.GetMouseButton(0)) // 왼쪽 마우스 누른 상태에서 드래그.
        {
            // y 값 설정
            rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

            // x 값 설정
            rotationX += Input.GetAxis("Mouse X") * sensitivityX;

            // x축 기준 y값, y축 기준 x값
            m_camera.transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
        }
    }
}
