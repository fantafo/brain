﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour {
	// MonoBehivior 클래스를 상속하고 T 매개변수로부터 다른 하나를 얻는다
   
	// T 클래스의 현재 인스턴스를 저장할 것이다.
	private static T _instance;

	// 앱이 현재 닫히고 있는 중인지 알기 위해 사용할 것이다.
	private static bool appIsClosing = false;

	// T의 public 인스턴스. 어디에서든 접근 가능하다.
	public static T Instance
	{
		// 오버라이드된 getter 메소드를 사용하다
		get
		{
			// 앱이 닫히는 중이라면,,
			if (appIsClosing)
			{
				// ... null을 반환하고, 더이상 진행하지 않는다
				return null;
			}

			// _instance가 할당되지 않았다면...
			if (_instance == null)
			{
				// 씬에 이미 인스턴스가 존재하늕지 확인하다.
				_instance = (T) FindObjectOfType(typeof(T));

				// 존재하지 않는다면? 새로 만든다
				if(_instance == null)
				{
					// 새로운 게임 오브젝트를 생성한다
					GameObject newSingleton = new GameObject();

					// 이것에 T 컴포넌트를 추가한다
					_instance = newSingleton.AddComponent<T>();

					// 이것의 이름을 T 클래스의 이름으로 바꾼다
					newSingleton.name = typeof(T).ToString();
				}
				// 이것을 DotDestroyOnLoad로 표시한다
				DontDestroyOnLoad(_instance);
			}
			// 마지막으로 _instance를 반환한다
			return _instance;
		}
	}

	// 모든 싱글톤 클래스에서 시작될 때
	public void Start()
	{
		// 씬안의 모든 싱글톤의 인스턴스를 얻는다
		T[] allInstances = FindObjectsOfType(typeof(T)) as T[];

		// 하나 이상 인스턴스가 존재한다면 
		if(allInstances.Length > 1)
		{
			// 발견된 인스턴스에 각각에 대해..
			foreach(T instanceToCheck in allInstances)
			{
				// 발견된 인스턴스가 현재 인스턴스가 아니라면
				if(instanceToCheck != Instance)
				{
					// 파괴한다
					Destroy(instanceToCheck.gameObject);
				}
			}
		}
		// 존재하는 인스턴스를 DontDestroyOnLoad로 표시한다
		DontDestroyOnLoad((T) FindObjectOfType(typeof(T)));
	}

	// 애플리케이션이 종료될 때
	void OnApplicationQuit()
	{
		// ...appIsClosing을 true로 설정한다
		appIsClosing = true;
	}
}
