﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Sequence08 : MonoBehaviour
{
    [SerializeField]
    private GameObject m_EightSequence;   // 8.시퀀스 오브젝트
    [SerializeField]
    private GameObject m_CameraContainer;   // 8.시퀀스 카메라 컨테이너
    [SerializeField]
    private GameObject m_CameraContainerInit;   // 루트 카메라 컨테이너
    [SerializeField]
    private GameObject m_LogoCanvasEnd;      // 로고 UI
    [SerializeField]
    private GameObject m_VRCameraUI;         // 카메라 UI

    // 로고
    private GameObject m_NHRILogo;
    private GameObject m_ScienceArtisanLogo;

    private void Awake()
    {
        m_NHRILogo = m_LogoCanvasEnd.transform.Find("Logo Panel").transform.Find("NHRI Logo").gameObject;
        m_ScienceArtisanLogo = m_LogoCanvasEnd.transform.Find("Logo Panel").transform.Find("ScienceArtisan Logo").gameObject;
    }


    public GameObject eightSequence
    {
        get { return m_EightSequence; }
    }

    // 8.시퀀스 시작 함수
    public void StartSequence8()
    {
        m_EightSequence.SetActive(true);  // 8.시퀀스 켜기

        // 메인카메라 컨테이너 붙이기 + 초기화
        GameManager.Instance.InitMainCamera(m_CameraContainer);

        // 트랙킹 초기화 
        GameManager.Instance.TrackingRecenter();

        // 7.씨퀀스 끄기
        GetComponent<Sequence07>().sevenSequence.SetActive(false);

        StartCoroutine(NarrationOnSQ8());
    }

    IEnumerator NarrationOnSQ8()
    {
        yield return new WaitForSeconds(1f);
        GameManager.Instance.NarrationOn(20);
        yield return new WaitForSeconds(12f);
        GameManager.Instance.NarrationOn(21);
        yield return null;
    }

    public void EndSequence()
    {
        // 종료

        GameManager.Instance.FadeOut(false);     // Fade out 후 Fade in 자동 실행
        GameManager.Instance.InitMainCamera(m_CameraContainerInit);        
        GameManager.Instance.bgmSound.DOFade(0f, 5f);
        m_VRCameraUI.SetActive(false);
        m_LogoCanvasEnd.SetActive(true);
        m_EightSequence.SetActive(false); // 8.시퀀스 끄기
    }

    public void ScienceArtisanLogoOn()
    {
        m_NHRILogo.SetActive(false);
        m_ScienceArtisanLogo.SetActive(true);
    }

    // 사이언스 아티산 로고 fade 후 어플 종료
    public void AppClose()
    {
        Application.Quit();
    }   
}
