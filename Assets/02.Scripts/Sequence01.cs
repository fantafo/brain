﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;


public class Sequence01 : MonoBehaviour
{
    [SerializeField]
    private GameObject m_OneSequence;   // 1.시퀀스 오브젝트
    [SerializeField]
    private GameObject m_CameraContainer;   // 1.시퀀스 카메라 컨테이너
    [SerializeField]
    private GameObject m_TargetPosition;    // 카메라 패스 타겟 위치
    [SerializeField]
    private GameObject m_Brain;     // 뇌 오브젝트
    [SerializeField]
    private float movingDuration =12f;       // moving 시간
    [SerializeField]
    private float logoFadeDuration = 5f;       // 로고 Fade out 시간
    [SerializeField]
    private Image m_logoImage;  
    [SerializeField]
    private Text m_logoText01;
    [SerializeField]
    private Text m_logoText02;
    [SerializeField]
    private GameObject m_DescriptionImage;
    [SerializeField]
    private GameObject m_DescriptionText;
       
    private Vector3 targetMovePosition;
    
    public GameObject oneSequence
    {
        get { return m_OneSequence; }
    }

    // 1.시퀀스 시작 함수
    public void StartSequence1()
    {
        m_OneSequence.SetActive(true);  // 1.시퀀스 켜기

        // 메인카메라 컨테이너 붙이기 + 초기화
        GameManager.Instance.InitMainCamera(m_CameraContainer);     
        
        // 트랙킹 초기화 
        GameManager.Instance.TrackingRecenter();

        targetMovePosition = m_Brain.GetComponent<Transform>().position;

        StartCoroutine(MoveTargetPosition());
    }

    IEnumerator MoveTargetPosition()
    {
        // BGM 1 시작
        GameManager.Instance.BGMOn(1); 

        yield return new WaitForSeconds(2f);

        // 뇌탐험 로고 Fade out
        m_logoImage.DOFade(0f, logoFadeDuration).SetEase(Ease.Linear);
        m_logoText01.DOFade(0f, logoFadeDuration).SetEase(Ease.Linear);
        m_logoText02.DOFade(0f, logoFadeDuration).SetEase(Ease.Linear);

        // 카메라 path 타겟 무빙 (1초 먼저 도착)
        m_TargetPosition.transform.DOLocalMove(targetMovePosition, movingDuration-1f).SetEase(Ease.InOutQuad);      
        // 뇌 회전
        m_Brain.transform.DOLocalRotate(new Vector3(0f, -90f, 20f), movingDuration).SetEase(Ease.InOutQuad);

        // 나레이션        
        GameManager.Instance.NarrationOn(1);         // 나레이션 1 play
        yield return new WaitForSeconds(5f);
        GameManager.Instance.NarrationOn(2);         // 나레이션 2 play
                
        // 브레인 설명 Fade in
        yield return new WaitForSeconds(1f);
        m_DescriptionImage.GetComponent<Image>().DOFade(1f, 2f).SetEase(Ease.Linear);
        m_DescriptionText.GetComponent<Text>().DOFade(1f, 2f).SetEase(Ease.Linear);

        // 브레인 설명 Fade out
        yield return new WaitForSeconds(5f);
        m_DescriptionImage.GetComponent<Image>().DOFade(0f, 1f).SetEase(Ease.Linear);
        m_DescriptionText.GetComponent<Text>().DOFade(0f, 1f).SetEase(Ease.Linear);
    }

    // 카메라 패스 후 호출 함수
    public void CameraPathFinish()
    {
        StartCoroutine(LoadSequence2());
    }

    // 2.시퀀스 호출
    IEnumerator LoadSequence2()
    {
        yield return null;

        GameManager.Instance.OnChangeSequence -= GetComponent<Sequence01>().StartSequence1;
        GameManager.Instance.OnChangeSequence += GetComponent<Sequence02>().StartSequence2;

        GameManager.Instance.FadeOut(true);     // Fade out 후 Fade in 자동 실행
    }
}
