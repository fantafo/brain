﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTF;


// App 시작 : Intro > logo > Sequence1
public class IntroLogo : MonoBehaviour
{
    [SerializeField]
    private GameObject m_VRCameraUI;    // 카메라 UI
    [SerializeField]
    private GameObject m_LogoCanvasStart;   // 로고 UI

    private GameObject m_NHRILogo;
    private GameObject m_ScienceArtisanLogo;

    void Awake()
    {
        m_NHRILogo =
            m_LogoCanvasStart.transform.Find("Logo Panel").transform.Find("NHRI Logo").gameObject;
        m_ScienceArtisanLogo =
            m_LogoCanvasStart.transform.Find("Logo Panel").transform.Find("ScienceArtisan Logo").gameObject;
    }

    void Start()
    {
        m_VRCameraUI.SetActive(false);     // Dotween에서 자동 시작
    }

    public void ScienceArtisanLogoOn()
    {
        m_NHRILogo.SetActive(false);
        m_ScienceArtisanLogo.SetActive(true);
    }

    // 시퀀스1 호출
    public void LoadSequence1()
    {
        m_ScienceArtisanLogo.SetActive(false);
        m_LogoCanvasStart.SetActive(false);

        if(PlayerInstance.lookMain.IsRoomMaster)
            m_VRCameraUI.SetActive(true);


        //pico 정면바라보기
        if (Pvr_UnitySDKManager.SDK != null)
        {
            if (Pvr_UnitySDKManager.pvr_UnitySDKSensor != null)
            {
                Pvr_UnitySDKManager.pvr_UnitySDKSensor.ResetUnitySDKSensor();
            }
            else
            {
                Pvr_UnitySDKManager.SDK.pvr_UnitySDKEditor.ResetUnitySDKSensor();
            }

        }

        GameManager.Instance.OnChangeSequence += GetComponent<Sequence01>().StartSequence1;
        //GameManager.Instance.OnChangeSequence += GetComponent<Sequence02>().StartSequence2;
        //GameManager.Instance.OnChangeSequence += GetComponent<Sequence03>().StartSequence3;
        //GameManager.Instance.OnChangeSequence += GetComponent<Sequence04>().StartSequence4;
        //GameManager.Instance.OnChangeSequence += GetComponent<Sequence05>().StartSequence5;
        //GameManager.Instance.OnChangeSequence += GetComponent<Sequence06>().StartSequence6;
        //GameManager.Instance.OnChangeSequence += GetComponent<Sequence07>().StartSequence7;
        //GameManager.Instance.OnChangeSequence += GetComponent<Sequence08>().StartSequence8;
        GameManager.Instance.FadeIn();
    }   
}
