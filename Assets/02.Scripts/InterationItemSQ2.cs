﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTF;
using FTF.Packet;


public class InterationItemSQ2 : InteractionItem
{
    public static Sequence02 m_Sequence02;
    
    
    // Use this for initialization
    void Awake()
    {
        base.Awake();

        m_Sequence02 = GameObject.FindObjectOfType<Sequence02>();
    }

    // 인터렉션 실행 함수
    public override void HandleSelectionComplete()
    {
        if (base.m_GazeOver && PlayerInstance.main.IsRoomMaster)
        {
            base.m_SelectionRadial.Hide(); // Active Radial 숨기기
            
            if (gameObject.name.Contains("frontal"))
            {
                Networker.Send(C_Brain.Index(0));
                //m_Sequence02.BrainFrontalAction();
            }
            else if (gameObject.name.Contains("occipital"))
            {
                Networker.Send(C_Brain.Index(1));
               // m_Sequence02.BrainOccipitalAction();
            }
            else if (gameObject.name.Contains("parietal"))
            {
                Networker.Send(C_Brain.Index(2));
               // m_Sequence02.BrainParietalAction();
            }
            else if (gameObject.name.Contains("temporal"))
            {
                Networker.Send(C_Brain.Index(3));
                //m_Sequence02.BrainTemporalAction();
            }
            else if (gameObject.name.Contains("hippo"))
            {
                Networker.Send(C_Brain.Index(4));
                //m_Sequence02.BrainHippoAction();
            }
            
            gameObject.SetActive(false);
        }
        
    }

}
