﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sequence05 : MonoBehaviour
{
    [SerializeField]
    private GameObject m_FiveSequence;   // 5.시퀀스 오브젝트
    [SerializeField]
    private GameObject m_CameraContainer;   // 5.시퀀스 카메라 컨테이너

    public GameObject fiveSequence
    {
        get { return m_FiveSequence; }
    }

    // 5.시퀀스 시작 함수
    public void StartSequence5()
    {
        m_FiveSequence.SetActive(true);  // 5.시퀀스 켜기

        // 메인카메라 컨테이너 붙이기 + 초기화
        GameManager.Instance.InitMainCamera(m_CameraContainer);

        // 트랙킹 초기화 
        GameManager.Instance.TrackingRecenter();

        // 4.씨퀀스 끄기
        GetComponent<Sequence04>().fourSequence.SetActive(false);

        StartCoroutine(NarrationOnSQ5());
    }

    IEnumerator NarrationOnSQ5()
    {
        GameManager.Instance.BGMOn(3);      // BGM3 
        GameManager.Instance.bgmSound.volume = 0.3f;
        GameManager.Instance.NarrationOn(14);    // 나레이션_14번
        yield return new WaitForSeconds(8f);
        GameManager.Instance.NarrationOn(15);
        //yield return new WaitForSeconds(10f);
        //SQ5PathFinish();

        yield return null;
    }

    public void SQ5PathFinish()
    {
        StartCoroutine(LoadSequence6());
    }

    // 6.시퀀스 호출
    IEnumerator LoadSequence6()
    {
        yield return new WaitForSeconds(GameManager.Instance.sequenceDuration);     // 대기

        GameManager.Instance.OnChangeSequence -= GetComponent<Sequence05>().StartSequence5;
        GameManager.Instance.OnChangeSequence += GetComponent<Sequence06>().StartSequence6;

        GameManager.Instance.FadeOut(true);     // Fade out 후 Fade in 자동 실행

        GameManager.Instance.NarrationOn(16);    // 나레이션_16번
        
    }
  
}
