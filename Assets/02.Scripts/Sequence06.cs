﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sequence06 : MonoBehaviour
{
    [SerializeField]
    private GameObject m_SixSequence;   // 6.시퀀스 오브젝트
    [SerializeField]
    private GameObject m_CameraContainer;   // 6.시퀀스 카메라 컨테이너

    public GameObject sixSequence
    {
        get { return m_SixSequence; }
    }

    // 6.시퀀스 시작 함수
    public void StartSequence6()
    {
        m_SixSequence.SetActive(true);  // 6.시퀀스 켜기

        // 메인카메라 컨테이너 붙이기 + 초기화
        GameManager.Instance.InitMainCamera(m_CameraContainer);

        // 트랙킹 초기화 
        GameManager.Instance.TrackingRecenter();

        // 5.씨퀀스 끄기
        GetComponent<Sequence05>().fiveSequence.SetActive(false);

        StartCoroutine(NarrationOnSQ6());
    }

    IEnumerator NarrationOnSQ6()
    {
        yield return new WaitForSeconds(4f);
        GameManager.Instance.NarrationOn(17);
        
        yield return null;
    }

    public void SQ6PathFinish()
    {
        StartCoroutine(LoadSequence7());
    }

    // 7.시퀀스 호출
    IEnumerator LoadSequence7()
    {
        yield return null;

        GameManager.Instance.OnChangeSequence -= GetComponent<Sequence06>().StartSequence6;
        GameManager.Instance.OnChangeSequence += GetComponent<Sequence07>().StartSequence7;

        GameManager.Instance.FadeOut(true);     // Fade out 후 Fade in 자동 실행
    }
   
}
