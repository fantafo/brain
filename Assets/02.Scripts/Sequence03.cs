﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Sequence03 : MonoBehaviour
{
    [SerializeField]
    private GameObject m_ThreeSequence;   // 3.시퀀스 오브젝트
    [SerializeField]
    private GameObject m_CameraContainer;   // 3.시퀀스 카메라 컨테이너
    [SerializeField]
    private GameObject m_Hand;
    [SerializeField]
    private GameObject m_FingerPointUI;
    [SerializeField]
    private GameObject m_RecticleUI;
    [SerializeField]
    private GameObject m_FirstCameraPath;
    [SerializeField]
    private GameObject m_SecondCameraPath;
    [SerializeField]
    private GameObject m_PathTarget;
    [SerializeField]
    private GameObject m_PathTargetgoal;
    [SerializeField]
    private GameObject m_IcePanel;
    
    private Animator m_Animator;
    
    public GameObject threeSequence
    {
        get { return m_ThreeSequence; }
    }

    // 3.시퀀스 시작 함수
    public void StartSequence3()
    {
        m_ThreeSequence.SetActive(true);  // 3.시퀀스 켜기

        // 메인카메라 컨테이너 붙이기 + 초기화
        GameManager.Instance.InitMainCamera(m_CameraContainer);

        // 트랙킹 초기화 
        GameManager.Instance.TrackingRecenter();

        // 2.씨퀀스 끄기
        GetComponent<Sequence02>().twoSequence.SetActive(false);

        // 애니메이터 
        m_Animator = m_Hand.GetComponent<Animator>();

        StartCoroutine(HandAnimStart());        
    }

    IEnumerator HandAnimStart()
    {
        GameManager.Instance.NarrationOn(10);    // 나레이션_10번
            

        // 나레이션이 끝날때 쯤 손가락 애니메이션 플레이
        yield return new WaitForSeconds(6f);
        m_Animator.SetBool("Action", true);
        //yield return new WaitForSeconds(1f);
        //m_FirstCameraPath.SetActive(true);

        // 애니메이션 끝난 후 
        yield return new WaitForSeconds(2f);
        GameManager.Instance.NarrationOn(11);    // 나레이션_11번

        // 카메라 이동 후 손가락 끝 감각 표시
        yield return new WaitForSeconds(2f);        
        m_FingerPointUI.SetActive(true);

        // 줌인 렉티클 활성화
        yield return new WaitForSeconds(6.5f);
        //GameManager.Instance.NarrationOn(12);    // 나레이션_12번
        m_RecticleUI.SetActive(true);
        //자동화
        SecondPathFinish();
    }

    // 손가락 끝으로 카메라 줌인 (Recticle 선택하면 호출 되는 함수) 
    public void ZoomIntoFinger()
    {
        // Path Target의 위치를 Ice판넬에서 손가락 끝으로 이동
        m_PathTarget.transform.DOMove(m_PathTargetgoal.transform.position, 2f).SetEase(Ease.Linear); 
        m_SecondCameraPath.SetActive(true);
        m_FingerPointUI.SetActive(false);        
    }

    // 카메라 줌인 후 호출 함수
    public void SecondPathFinish()
    {
        m_IcePanel.SetActive(false);
        //m_Hand.SetActive(false);
        StartCoroutine(LoadSequence4());
    }
    
    // 4.시퀀스 호출
    IEnumerator LoadSequence4()
    {
        yield return null;

        GameManager.Instance.OnChangeSequence -= GetComponent<Sequence03>().StartSequence3;
        GameManager.Instance.OnChangeSequence += GetComponent<Sequence04>().StartSequence4;

        GameManager.Instance.FadeOut(true);     // Fade out 후 Fade in 자동 실행
    }
}
