﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;
using FTF;
using FTF.Packet;

// 바라보는 대상 아이템에 넣는 클래스
// 상속 : 부모 클래스
public class InteractionItem : MonoBehaviour
{
    protected VRInteractiveItem m_InteractiveItem;
    protected SelectionRadial m_SelectionRadial;

    protected bool m_GazeOver;

    private GameObject m_Dum;

    protected void Awake()
    {
        m_Dum = GameObject.Find("LeftEye");

        m_InteractiveItem = GetComponent<VRInteractiveItem>();
        
        if (m_Dum.GetComponent<Camera>().enabled == true)
        {
            m_SelectionRadial = GameObject.Find("LeftEye").GetComponent<SelectionRadial>();
        }
        else
        {
            m_SelectionRadial = Camera.main.GetComponent<SelectionRadial>();
        }
    }

    // 이벤트 연결
    void OnEnable()
    {
        m_InteractiveItem.OnOver += HandleOver;
        m_InteractiveItem.OnOut += HandleOut;
        m_InteractiveItem.OnClick += HandleClick;
        m_InteractiveItem.OnDoubleClick += HandleDoubleClick;
        m_SelectionRadial.OnSelectionComplete += HandleSelectionComplete; // 완료 후 함수 호출
    }

    // 이벤트 해제
    void OnDisable()
    {
        m_InteractiveItem.OnOver -= HandleOver;
        m_InteractiveItem.OnOut -= HandleOut;
        m_InteractiveItem.OnClick -= HandleClick;
        m_InteractiveItem.OnDoubleClick -= HandleDoubleClick;
        m_SelectionRadial.OnSelectionComplete -= HandleSelectionComplete; // 완료 후 함수 호출
    }

    //Handle the Over event
    protected void HandleOver()
    {
        m_SelectionRadial.Show();
        m_SelectionRadial.HandleDown(); // Radial Bar 실행
        m_GazeOver = true;
    }

    //Handle the Out event
    protected void HandleOut()
    {
        m_SelectionRadial.Hide();
        m_SelectionRadial.HandleUp(); // Radial Bar 취소
        m_GazeOver = false;
    }


    //Handle the Click event
    protected void HandleClick()
    {
        Debug.Log("Show click state");
    }


    //Handle the DoubleClick event
    protected void HandleDoubleClick()
    {
        Debug.Log("Show double click");
    }

    // 모두 완료 후...
    public virtual void HandleSelectionComplete()
    {
        // If the user is looking at the rendering of the scene when the radial's selection finishes, activate the button.

    }
}
