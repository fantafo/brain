﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class Sequence02 : MonoBehaviour
{
    [SerializeField]
    private GameObject m_TwoSequence;   // 2.시퀀스 오브젝트
    [SerializeField]
    private GameObject m_CameraContainer;   // 2.시퀀스 카메라 컨테이너
    [SerializeField]
    private GameObject m_BrainNormal;   // Normal 브레인
    [SerializeField]
    private GameObject m_BrainFront;    // 전두엽 설명 브레인
    [SerializeField]
    private GameObject m_BrainBack;     // 후두엽 설명 브레인 
    [SerializeField]
    private GameObject m_BrainUpside;     // 두정엽 설명 브레인
    [SerializeField]
    private GameObject m_BrainDownside;     // 측두엽 설명 브레인
    [SerializeField]
    private GameObject m_BrainHippo;    // 해마 설명 브레인
    [SerializeField]
    private GameObject m_BrainHippoFront;   // 해마 설명 브레인 앞쪽
    [SerializeField]
    private GameObject m_Hippo;     // 해마 부분 
    [SerializeField]
    private GameObject m_Hippo2;     // 뇌하수체
    
    [SerializeField]
    private GameObject m_BrainHippoDesc;  // 해마 설명 UI
    [SerializeField]
    private GameObject m_BrainHippoDesc2;  // 해마 설명 UI2

    [SerializeField]
    private GameObject m_NextSequenceDesc;      // 다음 시퀀스 넘어가기 위한 설명 UI 

    [SerializeField]
    private GameObject m_RecticleImage;     // 렉티클 이미지3
    [SerializeField]
    private float m_MovingDuration = 5f;
        
    private Vector3 m_BrainOriginLocalRotation;     // 브레인 위치 및 회전 초기값
    private int m_RecticleCount = 4;    // 해마 Action으로 넘어가기 위한 렉티클 선택했던 횟수

    public GameObject twoSequence
    {
        get { return m_TwoSequence; }
    }

    public GameObject[] pan_Seq2;

    // 2.시퀀스 시작 함수
    public void StartSequence2()
    {
        m_TwoSequence.SetActive(true);  // 2.시퀀스 켜기

        // 메인카메라 컨테이너 붙이기 + 초기화
        GameManager.Instance.InitMainCamera(m_CameraContainer);

        // 트랙킹 초기화 
        GameManager.Instance.TrackingRecenter();

        // 1.씨퀀스 끄기
        GetComponent<Sequence01>().oneSequence.SetActive(false);

        // 렉티클 이미지 켜기
        //m_RecticleImage.SetActive(true);

        // 렉티클 나레이션 
        //StartCoroutine(NarrationSQ2());


        // Brain 초기 Rotation 값 
        m_BrainOriginLocalRotation = m_BrainNormal.transform.eulerAngles;

        //전두엽 설명
        BrainFrontalAction();
    }
    
    // 렉티클 나레이션 설명
    IEnumerator NarrationSQ2()
    {
        GameManager.Instance.NarrationOn(3);    // 나레이션_3번
        yield return null;
    }

    // 전두엽 설명
    public void BrainFrontalAction()
    {        
        StartCoroutine(BrainFrontalActionStart());
    }

    // 후두엽 설명
    public void BrainOccipitalAction()
    {
        StartCoroutine(BrainOccipitalActionStart());
    }

    // 두정엽 설명
    public void BrainParietalAction()
    {
        StartCoroutine(BrainParietalActionStart());
    }

    // 측두엽 설명
    public void BrainTemporalAction()
    {
        StartCoroutine(BrainTemporalActionStart());
    }

    // 해마 설명 및 Sequence 3 로드
    public void BrainHippoAction()
    {
        //StartCoroutine(BrainHippoActionStart());
        StartCoroutine(LoadSequence3());
    }

    // 전두엽
    IEnumerator BrainFrontalActionStart()
    {
        m_RecticleImage.SetActive(false);   // 렉티클 이미지 끄고
        m_RecticleCount -= 1;   // 렉티클 선택 횟수 1 차감
        m_BrainNormal.SetActive(false);     // 노멀 브레인 끄고
        m_BrainFront.SetActive(true);       // front 브레인 켜

        GameManager.Instance.NarrationOn(6);    // 나레이션_6번

        m_BrainFront.transform.DOLocalMoveZ(4f, m_MovingDuration).SetEase(Ease.InOutQuad);      // Dotween 무빙      
        yield return m_BrainFront.transform.DOLocalRotate(new Vector3(0f, -165f, 0f), m_MovingDuration)     
            .SetEase(Ease.Linear).WaitForCompletion();      // Dotween 회전        

        yield return new WaitForSeconds(6f);

        // 위치 및 머트리얼 초기화
        m_BrainFront.transform.DOLocalMoveZ(5f, m_MovingDuration).SetEase(Ease.InOutQuad);
        yield return m_BrainFront.transform.DOLocalRotate(m_BrainOriginLocalRotation, m_MovingDuration).SetEase(Ease.Linear).WaitForCompletion();

        // 트랙킹 초기화 
        GameManager.Instance.TrackingRecenter();

        if (m_RecticleCount > 0)
        {
            m_BrainFront.SetActive(false);
            m_BrainNormal.SetActive(true);
            m_RecticleImage.SetActive(true);
            //GameManager.Instance.NarrationOn(3);
            BrainOccipitalAction();
        }
        else
        {
            m_BrainFront.SetActive(false);
            StartCoroutine(BrainHippoActionStart()); 
        }                   
    }

    // 후두엽
    IEnumerator BrainOccipitalActionStart()
    {
        m_RecticleImage.SetActive(false);   // 렉티클 이미지 끄고
        m_RecticleCount -= 1;   // 렉티클 선택 횟수 1 차감
        m_BrainNormal.SetActive(false);     // 노멀 브레인 끄고
        m_BrainBack.SetActive(true);       // back 브레인 켜

        GameManager.Instance.NarrationOn(8);    // 나레이션_8번

        m_BrainBack.transform.DOLocalMoveZ(3f, m_MovingDuration).SetEase(Ease.InOutQuad);      // Dotween 무빙  
        yield return m_BrainBack.transform.DOLocalRotate(new Vector3(0f, -25f, 0f), m_MovingDuration)
            .SetEase(Ease.Linear).WaitForCompletion();      // Dotween 회전

        yield return new WaitForSeconds(3f);

        // 위치 및 머트리얼 초기화
        m_BrainBack.transform.DOLocalMoveZ(5f, m_MovingDuration).SetEase(Ease.InOutQuad);
        yield return m_BrainBack.transform.DOLocalRotate(m_BrainOriginLocalRotation, m_MovingDuration)
            .SetEase(Ease.Linear).WaitForCompletion();

        if (m_RecticleCount > 0)
        {
            m_BrainBack.SetActive(false);
            m_BrainNormal.SetActive(true);
            m_RecticleImage.SetActive(true);
            //GameManager.Instance.NarrationOn(3);
            BrainParietalAction();
        }
        else
        {
            m_BrainBack.SetActive(false);
            StartCoroutine(BrainHippoActionStart());
        }
    }
    
    // 두정엽
    IEnumerator BrainParietalActionStart()
    {
        m_RecticleImage.SetActive(false);   // 렉티클 이미지 끄고
        m_RecticleCount -= 1;   // 렉티클 선택 횟수 1 차감
        m_BrainNormal.SetActive(false);     // 노멀 브레인 끄고
        m_BrainUpside.SetActive(true);       // upside 브레인 켜

        GameManager.Instance.NarrationOn(7);    // 나레이션_7번

        m_BrainUpside.transform.DOLocalMoveZ(3f, m_MovingDuration).SetEase(Ease.InOutQuad);      // Dotween 무빙  
        yield return m_BrainUpside.transform.DOLocalRotate(new Vector3(0f, -70f, 60f), m_MovingDuration)
            .SetEase(Ease.Linear).WaitForCompletion();      // Dotween 회전

        yield return new WaitForSeconds(5f);

        // 위치 및 머트리얼 초기화
        m_BrainUpside.transform.DOLocalMoveZ(5f, m_MovingDuration).SetEase(Ease.InOutQuad);
        yield return m_BrainUpside.transform.DOLocalRotate(m_BrainOriginLocalRotation, m_MovingDuration)
            .SetEase(Ease.Linear).WaitForCompletion();       

        if (m_RecticleCount > 0)
        {
            m_BrainUpside.SetActive(false);
            m_BrainNormal.SetActive(true);
            m_RecticleImage.SetActive(true);
            //GameManager.Instance.NarrationOn(3);
            BrainTemporalAction();
        }
        else
        {
            m_BrainUpside.SetActive(false);
            StartCoroutine(BrainHippoActionStart());
        }
    }

    // 측두엽
    IEnumerator BrainTemporalActionStart()
    {
        m_RecticleImage.SetActive(false);   // 렉티클 이미지 끄고
        m_RecticleCount -= 1;   // 렉티클 선택 횟수 1 차감
        m_BrainNormal.SetActive(false);     // 노멀 브레인 끄고
        m_BrainDownside.SetActive(true);       // DownSide 브레인 켜

        GameManager.Instance.NarrationOn(9);    // 나레이션_9번

        m_BrainDownside.transform.DOLocalMoveZ(3f, m_MovingDuration).SetEase(Ease.InOutQuad);      // Dotween 무빙  
        yield return m_BrainDownside.transform.DOLocalRotate(new Vector3(0f, -80f, 0f), m_MovingDuration)
            .SetEase(Ease.Linear).WaitForCompletion();      // Dotween 회전

        yield return new WaitForSeconds(3f);

        // 위치 및 머트리얼 초기화
        m_BrainDownside.transform.DOLocalMoveZ(5f, m_MovingDuration).SetEase(Ease.InOutQuad);
        yield return m_BrainDownside.transform.DOLocalRotate(m_BrainOriginLocalRotation, m_MovingDuration)
            .SetEase(Ease.Linear).WaitForCompletion();

        if (m_RecticleCount > 0)
        {
            m_BrainDownside.SetActive(false);
            m_BrainNormal.SetActive(true);
            m_RecticleImage.SetActive(true);
            //GameManager.Instance.NarrationOn(3);
        }
        else
        {
            m_BrainDownside.SetActive(false);
            StartCoroutine(BrainHippoActionStart());
        } 
    }

    // 해마
    IEnumerator BrainHippoActionStart()
    {
        m_RecticleImage.SetActive(false);   // 렉티클 이미지 끄고
        m_BrainNormal.SetActive(false);     // 노멀 브레인 끄고
        m_BrainHippo.SetActive(true);       // Hippo 브레인 켜

        yield return new WaitForSeconds(1.0f);
        m_BrainHippoFront.transform.DOLocalMoveZ(-4.0f, 2.0f).SetEase(Ease.InOutQuad).WaitForCompletion();

        // 트랙킹 초기화 
        GameManager.Instance.TrackingRecenter();

        // 해마 설명
        yield return new WaitForSeconds(1.0f);
        GameManager.Instance.NarrationOn(4);    // 나레이션_4번
        m_BrainHippoDesc.SetActive(true);
        yield return new WaitForSeconds(14f);

        // 뇌하수체 설명
        m_Hippo.SetActive(false);
        m_Hippo2.SetActive(true);
        GameManager.Instance.NarrationOn(5);    // 나레이션_5번
        m_BrainHippoDesc2.SetActive(true);
        yield return new WaitForSeconds(10f);

        // 위치 초기화
        m_BrainHippoFront.transform.DOLocalMoveZ(0f, 2.0f).SetEase(Ease.InOutQuad).WaitForCompletion();
        yield return new WaitForSeconds(2f);
        // 타이틀 Canvas 등장
        m_NextSequenceDesc.SetActive(true);     // Text
        m_NextSequenceDesc.GetComponentInChildren<Text>().DOFade(1f, 1f).SetEase(Ease.InOutQuad).WaitForCompletion();
        m_BrainHippo.transform.DOLocalMoveZ(10f, 10f).SetEase(Ease.InOutQuad);

        yield return new WaitForSeconds(3f);
        m_NextSequenceDesc.GetComponentInChildren<Text>().DOFade(0f, 1f).SetEase(Ease.InOutQuad);
        StartCoroutine(LoadSequence3());
    }

    // 3.시퀀스 호출
    IEnumerator LoadSequence3()
    {
        yield return new WaitForSeconds(GameManager.Instance.sequenceDuration);     // 대기

        GameManager.Instance.OnChangeSequence -= GetComponent<Sequence02>().StartSequence2;
        GameManager.Instance.OnChangeSequence += GetComponent<Sequence03>().StartSequence3;

        GameManager.Instance.FadeOut(true);     // Fade out 후 Fade in 자동 실행
    }
}
