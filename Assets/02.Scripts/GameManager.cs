﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.VR;

// 전역 클래스 
public class GameManager : Singleton<GameManager>
{
    [SerializeField]
    private GameObject m_MainCamera;
    [SerializeField]
    private GameObject m_FadePanel;
    [SerializeField]
    private AudioSource m_BGMSound;
    [SerializeField]
    private AudioSource m_SFXSound;
    [SerializeField]
    private AudioSource m_NarrationSound;
    
    [SerializeField]
    private float m_FadeDuration = 2.0f;
    [SerializeField]
    private float m_FadeToFadeDuration = 0.5f;
    [SerializeField]
    private float m_SequenceDuration = 2.0f;
    [SerializeField]
    private float m_RadialSelectionDuration = 2f;

    public AudioSource bgmSound
    {
        get { return m_BGMSound; }
    }
    public float sequenceDuration
    {
        get { return m_SequenceDuration; }
    }
    public float radialSelectionDuration
    {
        get { return m_RadialSelectionDuration; }
    }

    public event Action OnChangeSequence;   // 시퀀스 전환 이벤트 델리게이트

    // Fade 관련 변수
    private Image m_FadeImage;
    private bool m_isAutoFade = false;
    private float m_FadeAlpha_1 = 1f;
    private float m_FadeAlpha_0 = 0f;
    private float m_FadeTime = 0f;

    void Awake()
    {
        // fade 이미지 가져오기
        m_FadeImage = m_FadePanel.GetComponent<Image>();
    }

    public void FadeIn()
    {
        StartCoroutine(FadeInBegin());
    }

    // Fade In 실행
    public IEnumerator FadeInBegin()
    {
        OnChangeSequence(); // 다음 시퀀스 실행

        yield return new WaitForSeconds(m_FadeToFadeDuration);  //  Fade 사이에 잠시 대기

        m_FadeTime = 0f;
        Color color = m_FadeImage.color;
        color.a = Mathf.Lerp(m_FadeAlpha_1, m_FadeAlpha_0, m_FadeTime); // 기본 세팅

        while (color.a > 0f)
        {
            m_FadeTime += Time.deltaTime / m_FadeDuration;
            color.a = Mathf.Lerp(m_FadeAlpha_1, m_FadeAlpha_0, m_FadeTime);
            m_FadeImage.color = color;

            yield return null;
        }
    }

    // Fade Out 시작
    public void FadeOut(bool isAutoFade)
    {
        m_isAutoFade = isAutoFade;  // 자동 Fade Out 실행 여부 
        StartCoroutine(FadeOutBegin());
    }

    // Fade Out 실행
    public IEnumerator FadeOutBegin()
    {
        m_FadeTime = 0f;
        Color color = m_FadeImage.color;
        color.a = Mathf.Lerp(m_FadeAlpha_0, m_FadeAlpha_1, m_FadeTime); // 기본 세팅

        while (color.a < 1f)
        {
            m_FadeTime += Time.deltaTime / m_FadeDuration;            
            color.a = Mathf.Lerp(m_FadeAlpha_0, m_FadeAlpha_1, m_FadeTime);
            m_FadeImage.color = color;

            yield return null;
        }

        if (m_isAutoFade)
        {
            FadeIn();
        }

        m_isAutoFade = false;
    }

    // BGM 출력하기
    public void BGMOn(int bgmNum)
    {
        AudioClip audioClip = Resources.Load<AudioClip>("BGM/" + "bgm" + bgmNum.ToString());
        m_BGMSound.clip = audioClip;
        m_BGMSound.Play();
    }

    // 나래이션 출력하기
    public void NarrationOn(int narrNum)
    {
        AudioClip audioClip = Resources.Load<AudioClip>("Narration Kor/" + narrNum.ToString());
        m_NarrationSound.clip = audioClip;
        m_NarrationSound.Play();
    }

    // 시퀀스 이동 시 카메라 리셋
    public void InitMainCamera(GameObject cameraContainer)
    {
        // 메일카메라 붙이기 + 초기화 
        m_MainCamera.transform.parent = cameraContainer.transform;
        m_MainCamera.transform.localPosition = Vector3.zero;
        m_MainCamera.transform.localRotation = Quaternion.identity;
        m_MainCamera.transform.localScale = Vector3.one;
    }

    public void TrackingRecenter()
    {
        InputTracking.Recenter();
    }
}
