100+ Litspheres for Mudbox (can be used with other apps/games that use litsphere shaders);
Check the litsphere_examples.jpg file for a preview of the materials.

note: Make sure to add Reflection Maps to shiny materials, like silver, gold, platinum, eye, pearl, shiny_stone, etc.


Litspheres created by Fatih Gurdal.

http://www.bb0x.com
http://bb0x.wordpress.com
fatih.gurdal@live.nl



Do not redistribute without permission, contact me at fatih.gurdal@live.nl if you wish to publish these.