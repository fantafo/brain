﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// 체크데이 클래스
public class CheckDay : MonoBehaviour
{
    [SerializeField]
    private bool m_IsCheckDay = true; // false일 때 체크데이는 실행 되지 않는다.
    [SerializeField]
    private int m_Year = 0; // 년도 입력
    [SerializeField]
    private int m_Month = 0; // 월 입력
    [SerializeField]
    private int m_Day = 0; // 일 입력
    [SerializeField]
    private string m_SceneName = "Main"; // 다음씬의 이름 입력
    [SerializeField]
    private GameObject m_CheckDayUILocalFail; // UI 오브젝트 클리어

    // false일 때 다음씬 실행 되지 않는다.
    private bool m_IsCheckDayAccept = false;

    // DateTime 매개변수에 static 넣어야 함.
    static int y = 0;
    static int m = 0;
    static int d = 0;

    // 유저입력, 현재시간
    private DateTime m_UserInputTime;
    private DateTime m_serverTime;
    private DateTime m_NowTime;

    void Start()
    {
        CheckDayStart();
    }

    // 클래스 시작 함수
    void CheckDayStart()
    {
        m_NowTime = DateTime.Now;
        //Debug.Log(Time_2.ToString());

        // 유저 입력값을 static 변수에 넣는다.
        y = m_Year;
        m = m_Month;
        d = m_Day;
        //Debug.Log(y.ToString() + m.ToString() + d.ToString());

        m_UserInputTime = new DateTime(y, m, d,23,59,59); //입력한 날의 23시 59분 59초까지 실행 가능
        //Debug.Log(m_UserInputTime.ToString());

        StartCoroutine(DownloadTimeRoutine());
    }

    IEnumerator DownloadTimeRoutine()
    {
        // 2. 구글 페이지 다운. 
        //    헤더에 시간 정보가 있다.
        WWW www = new WWW("http://www.google.com");

        // 3. 웹다운 기다림.
        yield return www;

        // 4. 오류가 없으면.
        if (string.IsNullOrEmpty(www.error))
        {
            // 5. 이런 형태로 결과가 온다.
            //    구글   : Mon, 24 Apr 2017 06:33:56 GMT,Mon, 24 Apr 2017 06:33:56 GMT,Mon, 24 Apr 2017 06:33:57 GMT
            //    네이버 : Mon, 24 Apr 2017 06:33:56 GMT,Mon, 24 Apr 2017 06:33:56 GMT
            //    다음  : Mon, 24 Apr 2017 06:33:56 GMT
            //    왜 이렇게 오는지 모르겠지만.. 반복되는 부분을 잘라서 쓰자.
            string timeHeader = www.responseHeaders["DATE"];

            int index = timeHeader.IndexOf("GMT") + 3;

            // 6. Mon, 24 Apr 2017 06:33:56 GMT
            string stringTime = timeHeader.Substring(0, index);

            // DateTime.Parser가 닷넷 사용 문화권에 맞게 자동으로 한국시간에 맞추어 +9해주는듯하다.
            // 하지만 표준시간 +9 해야된다는걸 인식하기위해서 다시 UTC타임으로 변환.
            DateTime dtNosp = DateTime.Parse(stringTime).ToUniversalTime();

            // 8. 스택틱으로 잡아둔다 앞으로 이걸계속 사용.
            //    한국시간으로 맞추기 위해 +9시간.
            //    시간 다운받기전까지 지나간 시간 빼준다.
            m_serverTime = dtNosp.AddHours(9).AddSeconds(-UnityEngine.Time.realtimeSinceStartup);

            DayCheckFuncNet(); //서버시간과 유저입력시간을 비교하는 함수
        }
        //네트워크 미연결
        else
        {
            yield return new WaitForSeconds(3.0f);
            DayCheckFuncLocal(); //디바이스시간과 유저입력시간을 비교하는 함수
        }

    }

    // 라이센스 비교 함수
    void DayCheckFuncNet()
    {
        // 날짜끼리 비교
        // 1 = m_UserInputTime이 크다. 
        // 0 = m_UserInputTime과 m_NowTime가 같다. 
        // -1 = m_NowTime가 크다
        int result = DateTime.Compare(m_UserInputTime, m_serverTime);

        if (result >= 0) //남은 기한이 디바이스 날짜보다 크거나 같으면 데이체크 성공
        {
            m_IsCheckDayAccept = true;
        }
        else
        {
            m_IsCheckDayAccept = false; //남은 기한이 디바이스 날짜보다 작으면 데이체크 실패
        }

        Debug.Log(result.ToString());
        Debug.Log(m_serverTime.ToString());

        LoadScene();
    }

    void DayCheckFuncLocal()
    {
        // 날짜끼리 비교
        // 1 = m_UserInputTime이 크다. 
        // 0 = m_UserInputTime과 m_NowTime가 같다. 
        // -1 = m_NowTime가 크다
        int result = DateTime.Compare(m_UserInputTime, m_NowTime);

        if (result >= 0) //남은 기한이 디바이스 날짜보다 크거나 같으면 데이체크 성공
        {
            m_IsCheckDayAccept = true;
        }
        else
        {
            m_IsCheckDayAccept = false; //남은 기한이 디바이스 날짜보다 작으면 데이체크 실패
        }

        Debug.Log(result.ToString());
        Debug.Log(m_serverTime.ToString());

        LoadScene();
    }

   
    // 씬 실행
    void LoadScene()
    {
        if (m_IsCheckDay) // m_IsCheckDay이 True일 때만 실행
        {
            if (m_IsCheckDayAccept)
            {
                SceneManager.LoadScene(m_SceneName);
            }
            else
            {
                StartCoroutine(EndGame());
            }
        }
        else // CheckDayOn이 False일 때 다음 무조건 씬 로드
        {
            SceneManager.LoadScene(m_SceneName);
        }
    }

    // 앱 종료 함수
    IEnumerator EndGame()
    {
        m_CheckDayUILocalFail.SetActive(true);

        // 일정 시간 지난 후 자동 종료
        yield return new WaitForSeconds(5.0f);

#if UNITY_EDITOR // 유니티 에디터에서만 실행

        UnityEditor.EditorApplication.isPlaying = false;

#else // 안드로이드, 아이폰 실행

                Application.Quit();
#endif
    }
}

