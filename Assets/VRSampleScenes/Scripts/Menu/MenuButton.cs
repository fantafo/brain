using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using VRStandardAssets.Utils;

namespace VRStandardAssets.Menu
{
    // This script is for loading scenes from the main menu.
    // Each 'button' will be a rendering showing the scene
    // that will be loaded and use the SelectionRadial.
    // 이 스크립트는 기본 메뉴에서 장면을 로드하기 위한 것.
    // 각 '버튼'은 장면을 보여주는 렌더링이 될 것.
    // 로드되고 SelectionRadial을 사용합니다.
    public class MenuButton : MonoBehaviour
    {
        public event Action<MenuButton> OnButtonSelected;   // This event is triggered when the selection of the button has finished.

        [SerializeField] private string m_SceneToLoad;       // The name of the scene to load.
        [SerializeField] private VRCameraFade m_CameraFade;  // This fades the scene out when a new scene is about to be loaded.
        [SerializeField] private SelectionRadial m_SelectionRadial; // This controls when the selection is complete.
        [SerializeField] private VRInteractiveItem m_InteractiveItem; // The interactive item for where the user should click to load the level.

        private bool m_GazeOver;                                            // Whether the user is looking at the VRInteractiveItem currently.

        // VRInteractiveItem 이벤트 연결
        private void OnEnable ()
        {
            m_InteractiveItem.OnOver += HandleOver;
            m_InteractiveItem.OnOut += HandleOut;
            m_SelectionRadial.OnSelectionComplete += HandleSelectionComplete; // 완료 후 함수 호출
        }


        private void OnDisable ()
        {
            m_InteractiveItem.OnOver -= HandleOver;
            m_InteractiveItem.OnOut -= HandleOut;
            m_SelectionRadial.OnSelectionComplete -= HandleSelectionComplete;
        }
        
        // 오브젝트 바라볼 때, Radial 보이기
        // SelectionRadial에서 VRInput의 HandleDown으로 Radial Acton 진행 중.
        private void HandleOver()
        {
            // When the user looks at the rendering of the scene, show the radial.
            m_SelectionRadial.Show();

            m_GazeOver = true;
        }

        // 오브젝트 그만 볼 때, Radial 없애기
        private void HandleOut()
        {
            // When the user looks away from the rendering of the scene, hide the radial.
            m_SelectionRadial.Hide();

            m_GazeOver = false;
        }

        // 모두 완료 후.....
        private void HandleSelectionComplete()
        {
            // If the user is looking at the rendering of the scene when the radial's selection finishes, activate the button.
            if(m_GazeOver)
                StartCoroutine (ActivateButton());
        }


        private IEnumerator ActivateButton()
        {
            // If the camera is already fading, ignore.
            if (m_CameraFade.IsFading)
                yield break;

            // If anything is subscribed to the OnButtonSelected event, call it.
            if (OnButtonSelected != null)
                OnButtonSelected(this);

            // Wait for the camera to fade out.
            yield return StartCoroutine(m_CameraFade.BeginFadeOut(true));

            // Load the level.
            SceneManager.LoadScene(m_SceneToLoad, LoadSceneMode.Single);
        }
    }
}