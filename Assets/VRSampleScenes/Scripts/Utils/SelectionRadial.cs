using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

namespace VRStandardAssets.Utils
{
    // This class is used to control a radial bar that fills
    // up as the user holds down the Fire1 button.  When it has
    // finished filling it triggers an event.  It also has a
    // coroutine which returns once the bar is filled.
    // 이 클래스는 Radial Bar를 컨트롤.
    // 사용자가 Fire1 버튼을 계속 누르고 있을 때,
    // 완료되면 이벤트 트리거. 또한
    // bar가 채워지면 반환되는 coroutine.
    public class SelectionRadial : MonoBehaviour
    {
        public event Action OnSelectionComplete;   //이 이벤트는 Radial이 채워질 때 트리거됨.

        [SerializeField]
        private bool m_HideOnStart = true; // Whether or not the bar should be visible at the start.
        [SerializeField]
        private Image m_Selection; // Radial Image
        [SerializeField]
        private VRInput m_VRInput;  // Reference to the VRInput so that input events can be subscribed to.

        private Coroutine m_SelectionFillRoutine; // 입력에 따라 Radial 코루틴을 시작하고 중지하는 데 사용됨.
        private bool m_IsSelectionRadialActive; // 현재 Radial 진행 중? Whether or not the bar is currently useable.
        private bool m_RadialFilled; // Radial이 끝났는지? Used to allow the coroutine to wait for the bar to fill.

        private float m_SelectionDuration; // 라디얼 셀렉션 대기 시간

        // Radial Bar가 채워지는 시간 public 변수
        public float SelectionDuration { get { return m_SelectionDuration; } }

        private void Awake()
        {
            // 라디얼 셀렉션 대기 시간 초기화
            m_SelectionDuration = GameManager.Instance.radialSelectionDuration;
        }

        // VRInput 이벤트 연결.
        private void OnEnable()
        {
            m_VRInput.OnDown += HandleDown; // 누를 때 순간! : Radial Bar가 채워지기 시작.
            m_VRInput.OnUp += HandleUp; // 누르고 뗀 후 순간! : Radial Bar가 취소.

        }

        // VRInput 이벤트 해제.
        private void OnDisable()
        {
            m_VRInput.OnDown -= HandleDown;
            m_VRInput.OnUp -= HandleUp;
        }

        private void Start()
        {
            // Setup the radial to have no fill at the start and hide if necessary.
            // 초기화
            m_Selection.fillAmount = 0f;

            // 시작은 끄고 한다.
            if (m_HideOnStart)
                Hide();
        }

        // Radial Image 보이기
        // Radial Action 시작
        public void Show()
        {
            m_Selection.gameObject.SetActive(true);
            m_IsSelectionRadialActive = true;
        }

        public void Hide()
        {
            m_Selection.gameObject.SetActive(false);
            m_IsSelectionRadialActive = false;

            // This effectively resets the radial for when it's shown again.
            m_Selection.fillAmount = 0f;
        }

        // VRInput OnDown : Radial Bar 채워지기 시작
        public void HandleDown()
        {
            // If the radial is active start filling it.
            if (m_IsSelectionRadialActive)
            {
                m_SelectionFillRoutine = StartCoroutine(FillSelectionRadial());
            }
        }

        // VRInput OnUp : Radial Bar 취소.
        public void HandleUp()
        {
            // If the radial is active stop filling it and reset it's amount.
            if (m_IsSelectionRadialActive)
            {
                if (m_SelectionFillRoutine != null)
                    StopCoroutine(m_SelectionFillRoutine);

                m_Selection.fillAmount = 0f;
            }
        }

        // Radial Action 함수 (채우기)
        private IEnumerator FillSelectionRadial()
        {
            // At the start of the coroutine, the bar is not filled.
            m_RadialFilled = false;

            // Create a timer and reset the fill amount.
            float timer = 0f;
            m_Selection.fillAmount = 0f;

            // This loop is executed once per frame until the timer exceeds the duration.
            // 이 루프는 타이머가 지속 시간을 초과 할 때까지 프레임 당 한 번 실행.
            while (timer < m_SelectionDuration)
            {
                // The image's fill amount requires a value from 0 to 1 so we normalise the time.
                m_Selection.fillAmount = timer / m_SelectionDuration;

                // Increase the timer by the time between frames and wait for the next frame.
                timer += Time.deltaTime;
                yield return null;
            }

            // When the loop is finished set the fill amount to be full.
            // 다 찼다!
            m_Selection.fillAmount = 1f;

            // Turn off the radial so it can only be used once.
            // 작업 끝
            m_IsSelectionRadialActive = false;

            // The radial is now filled so the coroutine waiting for it can continue.
            // 다 채움
            m_RadialFilled = true;

            // If there is anything subscribed to OnSelectionComplete call it.
            // 다 채우고 난 후 아래 이벤트와 연결된 함수 호출
            if (OnSelectionComplete != null)
                OnSelectionComplete();
        }

        // Radial Action이 진행되는 동안 대기
        // Radial Image를 켜고, 끄는 역할
        public IEnumerator WaitForSelectionRadialToFill()
        {
            // Set the radial to not filled in order to wait for it.
            // 기다리지 않기 위해 방사형을 채우지 않도록 설정.
            m_RadialFilled = false;

            // Make sure the radial is visible and usable.
            Show();

            // Check every frame if the radial is filled.
            while (!m_RadialFilled)
            {
                yield return null;
            }

            // Once it's been used make the radial invisible.
            Hide();
        }
    }
}