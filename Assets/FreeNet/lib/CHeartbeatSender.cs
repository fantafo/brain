﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace FreeNet
{
    class CHeartbeatSender
    {
        CUserToken server;
        Timer timer_heartbeat;
        uint interval;

        float elapsed_time;


        public CHeartbeatSender(CUserToken server, uint interval)
        {
            this.server = server;
            this.interval = interval;
            this.timer_heartbeat = new Timer(this.on_timer, null, Timeout.Infinite, this.interval * 1000);
        }


        void on_timer(object state)
        {
            send();
        }


        void send()
        {
            //CPacket msg = CPacket.create((short)CUserToken.SYS_UPDATE_HEARTBEAT);
            //this.server.send(msg);


            //SYS_B_UPDATE_HEARTBEAT 221
            short packet_size = (short)3;
            byte[] packet = new byte[packet_size];
            //패킷사이즈를 byte[]로 변환후 packet에 저장
            BitConverter.GetBytes(packet_size).CopyTo(packet, 0);
            //프로토콜
            packet[2] = (byte)CUserToken.SYS_B_UPDATE_HEARTBEAT;
            this.server.send(new ArraySegment<byte>(packet, 0, packet.Length));

        }


        public void update(float time)
        {
            this.elapsed_time += time;
            if (this.elapsed_time < this.interval)
            {
                return;
            }

            this.elapsed_time = 0.0f;
            send();
        }


        public void stop()
        {
            this.elapsed_time = 0;
            this.timer_heartbeat.Change(Timeout.Infinite, Timeout.Infinite);
        }


        public void play()
        {
            this.elapsed_time = 0;
            this.timer_heartbeat.Change(0, this.interval * 1000);
        }
    }
}
