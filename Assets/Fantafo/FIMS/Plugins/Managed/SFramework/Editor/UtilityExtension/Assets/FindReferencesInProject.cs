﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using System.Text;
using System.Threading;

/// <summary>
/// Author : 정상철
/// </summary>
namespace Sions.Editors
{
    public class FindReferencesInProject
    {
        public class Data
        {
            public Object obj;
            public string path;
            public string guid;
            public List<string> dpendencyPath = new List<string>();
        }


        public static void ClearConsole()
        {
            // This simply does "LogEntries.Clear()" the long way:
            var logEntries = System.Type.GetType("UnityEditorInternal.LogEntries,UnityEditor.dll");
            var clearMethod = logEntries.GetMethod("Clear", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
            clearMethod.Invoke(null, null);
        }


        [MenuItem("Assets/Find References In Project/Show Path")]
        public static void DoFind()
        {
            doFind(false);
        }
        [MenuItem("Assets/Find References In Project/Show Path and Connect")]
        public static void DoFindConnection()
        {
            doFind(true);
        }

        static void doFind(bool connectObject)
        {
            if (!Selection.activeObject)
                return;

            List<Data> datas = new List<Data>();
            foreach (var go in Selection.objects)
            {
                string path = AssetDatabase.GetAssetPath(go);
                string guid = AssetDatabase.AssetPathToGUID(path);
                var data = new Data()
                {
                    obj = go,
                    guid = "guid: " + guid,
                    path = path.ToLower()
                };
                datas.Add(data);
            }

            System.DateTime start = System.DateTime.Now;
            SearchStart(Application.dataPath, datas, () =>
            {
                try
                {
                    List<Object> selections = new List<Object>();

                    ClearConsole();
                    foreach (var data in datas)
                    {
                        Debug.Log("---------------------------------");
                        Debug.Log("<color=orange>" + data.obj.name + " (" + data.dpendencyPath.Count + ") </color>", data.obj);

                        foreach (var dPath in data.dpendencyPath)
                        {
                            var path = dPath.Replace('\\', '/').Substring(Application.dataPath.Length - 6);

                            if (connectObject)
                            {
                                Object asset = AssetDatabase.LoadMainAssetAtPath(path);
                                selections.Add(asset);
                                Debug.Log(path, asset);
                            }
                            else
                            {
                                Debug.Log(path);
                            }
                        }
                    }
                    Debug.Log("---------------------------------");
                    Debug.Log("<color=white>Complete " + (System.DateTime.Now - start).TotalMilliseconds + "ms</color>");

                    Selection.objects = selections.ToArray();
                }
                catch (System.Exception e)
                {
                    Debug.LogException(e);
                }
            });
        }


        [MenuItem("Assets/Find References In Project/Select/Dont Dependencies")]
        public static void DoSelectDontDependencies()
        {
            if (!Selection.activeObject)
                return;

            List<Data> datas = new List<Data>();
            //List<string> paths = new List<string>();
            foreach (var go in Selection.objects)
            {
                string path = AssetDatabase.GetAssetPath(go);
                string guid = AssetDatabase.AssetPathToGUID(path);
                var data = new Data()
                {
                    obj = go,
                    guid = "guid: " + guid,
                    path = path.ToLower()
                };
                datas.Add(data);
            }

            System.DateTime start = System.DateTime.Now;
            SearchStart(Application.dataPath, datas, () =>
            {
                List<Object> selections = new List<Object>();
                foreach (var data in datas)
                {
                    if (data.dpendencyPath.Count == 0)
                    {
                        selections.Add(data.obj);
                    }
                }
                Selection.objects = selections.ToArray();

                Debug.Log("<color=white>Complete (" + selections.Count + ") " + (System.DateTime.Now - start).TotalMilliseconds + "ms</color>");
            });
        }



        ////////////////////////////////////////////////////////////////////////////
        //                                                                        //
        //                           Threading                                    //
        //                                                                        //
        ////////////////////////////////////////////////////////////////////////////

        static Thread thread;
        static bool IsSearch()
        {
            return thread != null;
        }
        static void SearchStart(string dirPath, List<Data> datas, System.Action act)
        {
            if (IsSearch())
            {
                Debug.LogWarning("Can't. Now Running.");
                return;
            }

            delayAction = null;
            EditorApplication.update += ThreadInvoker;
            thread = new Thread(new ThreadStart(() =>
            {
                try
                {
                    Search(dirPath, datas);
                    delayAction = act;
                }
                finally
                {
                    thread = null;
                }
            }));
            thread.Priority = System.Threading.ThreadPriority.Highest;
            thread.Start();
        }
        static System.Action delayAction = null;
        static void ThreadInvoker()
        {
            if (delayAction != null)
            {
                delayAction();
                EditorApplication.update -= ThreadInvoker;
            }
        }


        static void Search(string dirPath, List<Data> datas)
        {
            foreach (string filePath in Directory.GetFiles(dirPath))
            {
                string name = filePath.ToLower().Replace('\\', '/');
                int startIndex = name.IndexOf("assets/");
                name = name.Substring(startIndex);

                if (!datas.Contains(data => data.path != name))
                {
                    continue;
                }

                int extIndex = name.LastIndexOf('.');
                if (extIndex == -1)
                    continue;

                switch (name.Substring(extIndex + 1))
                {
                    case "prefab":
                    case "mat":
                    case "unity":
                    case "controller":
                    case "anim":
                        Check(filePath, datas);
                        break;

                    case "meta":
                        Check(filePath, datas, 60);
                        break;
                }
            }

            foreach (string subPath in Directory.GetDirectories(dirPath))
            {
                Search(subPath, datas);
            }
        }
        static void Check(string filePath, List<Data> datas, int skip = -1)
        {
            var txt = File.ReadAllText(filePath, Encoding.UTF8);
            if (filePath.EndsWith(".meta"))
                filePath = filePath.Substring(0, filePath.Length - 5);

            for (int i = 0, len = datas.Count; i < len; i++)
            {
                if (txt.IndexOf(datas[i].guid) > skip)
                {
                    datas[i].dpendencyPath.Add(filePath);
                }
            }
        }
    }
}