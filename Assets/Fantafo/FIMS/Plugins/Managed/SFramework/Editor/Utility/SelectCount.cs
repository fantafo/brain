﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

/// <summary>
/// Author : 정상철
/// </summary>
namespace Sions.Editors
{
    public class SelectInformationWindow : EditorWindow
    {
        [MenuItem("SF/Selected Count", priority = 0x1002)]
        public static void ShowWindow()
        {
            Debug.Log(Selection.gameObjects.Length);
            EditorWindow.GetWindowWithRect<SelectInformationWindow>(new Rect(0, 0, 100, 50), true, "Count", false);
        }

        void Start()
        {
            Selection.selectionChanged += Repaint;
        }

        void OnDestroy()
        {
            Selection.selectionChanged -= Repaint;
        }

        void OnGUI()
        {
            GUILayout.Label("Count : " + Selection.gameObjects.Length);
        }
    }
}