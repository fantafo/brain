﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[AddComponentMenu("SF/Tracker/Billboard")]
public class Billboard : AbsTracker
{
    [Space]
    public Vector3 offset;

    public override void OnReposition()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying && Camera.main == null)
            return;
#endif
        transform.rotation = Quaternion.LookRotation(transform.position - Camera.main.transform.position) * Quaternion.Euler(offset);
    }
}