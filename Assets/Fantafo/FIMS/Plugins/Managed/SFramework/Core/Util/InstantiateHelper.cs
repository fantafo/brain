﻿using System;
using System.Collections;
using UnityEngine;
using UObject = UnityEngine.Object;
using URandom = UnityEngine.Random;

public class InstantiateHelper
{
    public const float SKIP_INSTANTIATE_TIME = 7;

    public static IEnumerator Instantiate(UObject obj, int count, Action<UObject> onCreate)
    {
        DateTime dt = DateTime.Now;

        for (int i = 0; i < count; i++)
        {
            UObject o = GameObject.Instantiate(obj);
            if (onCreate != null)
                onCreate(o);

            if ((DateTime.Now - dt).TotalMilliseconds > SKIP_INSTANTIATE_TIME)
            {
                yield return null;
                dt = DateTime.Now;
            }
        }
    }
}