﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using UnityEngine;

namespace SFramework.TweenAction
{
    [Serializable]
    public class ActionAlphaMaterial : ComponentActionDiffFloat<Material, Color>
    {
        public override void Init()
        {
            _current = _target.color;
            SetFrom(_current.a);
        }

        public override void SetFrom(float v)
        {
            base.SetFrom(v);
            _current.a = v;
        }

        public override void Update(float ratio)
        {
            _current.a = from + (diff * ratio);
            _target.color = _current;
        }
    }
    [Serializable]
    public class ActionAlphaVertexMesh : ComponentActionDiffFloat<Mesh, Color>
    {
        Color32[] colors;

        public override void Init()
        {
            colors = new Color32[_target.vertexCount];

            _current = _target.colors32[0];
            SetFrom(_current.a);
        }

        public override void SetFrom(float v)
        {
            base.SetFrom(v);
            _current.a = v;
        }

        public override void Update(float ratio)
        {
            _current.a = from + (diff * ratio);

            for (int i = 0; i < colors.Length; i++)
                colors[i] = _current;

            _target.colors32 = colors;
        }
    }
#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
    [Serializable]
    public class ActionAlphaGraphic : ComponentActionDiffFloat<UnityEngine.UI.Graphic, Color>
    {
        public override void Init()
        {
            _current = _target.color;
            SetFrom(_current.a);
        }

        public override void SetFrom(float v)
        {
            base.SetFrom(v);
            _current.a = v;
        }

        public override void Update(float ratio)
        {
            _current.a = from + (diff * ratio);
            _target.color = _current;
        }
    }
    [Serializable]
    public class ActionAlphaCanvasGroup : ComponentActionDiffFloat<CanvasGroup, float>
    {
        public override void Init()
        {
            _current = _target.alpha;
            SetFrom(_current);
        }

        public override void SetFrom(float v)
        {
            base.SetFrom(v);
            _current = v;
        }

        public override void Update(float ratio)
        {
            _current = from + (diff * ratio);
            _target.alpha = _current;
        }
    }
#endif
}