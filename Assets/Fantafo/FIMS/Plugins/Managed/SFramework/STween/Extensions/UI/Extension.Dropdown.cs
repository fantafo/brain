﻿#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
using System;
using UnityEngine;
using UnityEngine.UI;
using SFramework.TweenAction;

public static class STweenDropdownExtension
{
    private static readonly Action<Dropdown, int> _changeDropdownValue = __changeDropdownValue;
    private static void __changeDropdownValue(Dropdown dropdown, int val) { dropdown.value = val; }
    public static STweenState twnValue(this Dropdown dropdown, int to, int duration)
    {
        return STween.Play<ActionIntObject<Dropdown>, Dropdown, int>(dropdown, dropdown.value, to, duration, _changeDropdownValue);
    }
    public static STweenState twnValue(this Dropdown dropdown, int from, int to, int duration)
    {
        return STween.Play<ActionIntObject<Dropdown>, Dropdown, int>(dropdown, from, to, duration, _changeDropdownValue);
    }
}
#endif