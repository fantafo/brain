﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SFramework.TweenAction;

public static class STweenColliderExtension
{
    private static readonly Action<BoxCollider, Vector3> _changeCenter = __changeCenter;
    private static void __changeCenter(BoxCollider boxCollider, Vector3 val) { boxCollider.center = val; }
    public static STweenState twnCenter(this BoxCollider boxCollider, Vector3 to, float duration)
    {
        return STween.Play<ActionVector3Object<BoxCollider>, BoxCollider, Vector3>(boxCollider, boxCollider.center, to, duration, _changeCenter);
    }
    public static STweenState twnCenter(this BoxCollider boxCollider, Vector3 from, Vector3 to, float duration)
    {
        return STween.Play<ActionVector3Object<BoxCollider>, BoxCollider, Vector3>(boxCollider, from, to, duration, _changeCenter);
    }

    private static readonly Action<BoxCollider, Vector3> _changeSize = __changeSize;
    private static void __changeSize(BoxCollider boxCollider, Vector3 val) { boxCollider.size = val; }
    public static STweenState twnSize(this BoxCollider boxCollider, Vector3 to, float duration)
    {
        return STween.Play<ActionVector3Object<BoxCollider>, BoxCollider, Vector3>(boxCollider, boxCollider.size, to, duration, _changeSize);
    }
    public static STweenState twnSize(this BoxCollider boxCollider, Vector3 from, Vector3 to, float duration)
    {
        return STween.Play<ActionVector3Object<BoxCollider>, BoxCollider, Vector3>(boxCollider, from, to, duration, _changeSize);
    }




    private static readonly Action<SphereCollider, float> _changeSphereColliderRadius = __changeSphereColliderRadius;
    private static void __changeSphereColliderRadius(SphereCollider sphereCollider, float val) { sphereCollider.radius = val; }
    public static STweenState twnRadius(this SphereCollider sphereCollider, float to, float duration)
    {
        return STween.Play<ActionFloatObject<SphereCollider>, SphereCollider, float>(sphereCollider, sphereCollider.radius, to, duration, _changeSphereColliderRadius);
    }
    public static STweenState twnRadius(this SphereCollider sphereCollider, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<SphereCollider>, SphereCollider, float>(sphereCollider, from, to, duration, _changeSphereColliderRadius);
    }


    private static readonly Action<SphereCollider, Vector3> _changeSphereColliderCenter = __changeSphereColliderCenter;
    private static void __changeSphereColliderCenter(SphereCollider sphereCollider, Vector3 val) { sphereCollider.center = val; }
    public static STweenState twnCenter(this SphereCollider sphereCollider, Vector3 to, float duration)
    {
        return STween.Play<ActionVector3Object<SphereCollider>, SphereCollider, Vector3>(sphereCollider, sphereCollider.center, to, duration, _changeSphereColliderCenter);
    }
    public static STweenState twnCenter(this SphereCollider sphereCollider, Vector3 from, Vector3 to, float duration)
    {
        return STween.Play<ActionVector3Object<SphereCollider>, SphereCollider, Vector3>(sphereCollider, from, to, duration, _changeSphereColliderCenter);
    }






    private static readonly Action<CapsuleCollider, float> _changeCapsuleColliderRadius = __changeCapsuleColliderRadius;
    private static void __changeCapsuleColliderRadius(CapsuleCollider capsuleCollider, float val) { capsuleCollider.radius = val; }
    public static STweenState twnRadius(this CapsuleCollider capsuleCollider, float to, float duration)
    {
        return STween.Play<ActionFloatObject<CapsuleCollider>, CapsuleCollider, float>(capsuleCollider, capsuleCollider.radius, to, duration, _changeCapsuleColliderRadius);
    }
    public static STweenState twnRadius(this CapsuleCollider capsuleCollider, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<CapsuleCollider>, CapsuleCollider, float>(capsuleCollider, from, to, duration, _changeCapsuleColliderRadius);
    }


    private static readonly Action<CapsuleCollider, float> _changeCapsuleColliderHeight = __changeCapsuleColliderHeight;
    private static void __changeCapsuleColliderHeight(CapsuleCollider capsuleCollider, float val) { capsuleCollider.height = val; }
    public static STweenState twnHeight(this CapsuleCollider capsuleCollider, float to, float duration)
    {
        return STween.Play<ActionFloatObject<CapsuleCollider>, CapsuleCollider, float>(capsuleCollider, capsuleCollider.height, to, duration, _changeCapsuleColliderHeight);
    }
    public static STweenState twnHeight(this CapsuleCollider capsuleCollider, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<CapsuleCollider>, CapsuleCollider, float>(capsuleCollider, from, to, duration, _changeCapsuleColliderHeight);
    }


    private static readonly Action<CapsuleCollider, Vector3> _changeCapsuleColliderCenter = __changeCapsuleColliderCenter;
    private static void __changeCapsuleColliderCenter(CapsuleCollider capsuleCollider, Vector3 val) { capsuleCollider.center = val; }
    public static STweenState twnCenter(this CapsuleCollider capsuleCollider, Vector3 to, float duration)
    {
        return STween.Play<ActionVector3Object<CapsuleCollider>, CapsuleCollider, Vector3>(capsuleCollider, capsuleCollider.center, to, duration, _changeCapsuleColliderCenter);
    }
    public static STweenState twnCenter(this CapsuleCollider capsuleCollider, Vector3 from, Vector3 to, float duration)
    {
        return STween.Play<ActionVector3Object<CapsuleCollider>, CapsuleCollider, Vector3>(capsuleCollider, from, to, duration, _changeCapsuleColliderCenter);
    }








    private static readonly Action<WheelCollider, float> _changeWheelColliderBrakeTorque = __changeWheelColliderBrakeTorque;
    private static void __changeWheelColliderBrakeTorque(WheelCollider wheelCollider, float val) { wheelCollider.brakeTorque = val; }
    public static STweenState twnBrakeTorque(this WheelCollider wheelCollider, float to, float duration)
    {
        return STween.Play<ActionFloatObject<WheelCollider>, WheelCollider, float>(wheelCollider, wheelCollider.brakeTorque, to, duration, _changeWheelColliderBrakeTorque);
    }
    public static STweenState twnBrakeTorque(this WheelCollider wheelCollider, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<WheelCollider>, WheelCollider, float>(wheelCollider, from, to, duration, _changeWheelColliderBrakeTorque);
    }


    private static readonly Action<WheelCollider, float> _changeWheelColliderForceAppPointDistance = __changeWheelColliderForceAppPointDistance;
    private static void __changeWheelColliderForceAppPointDistance(WheelCollider wheelCollider, float val) { wheelCollider.forceAppPointDistance = val; }
    public static STweenState twnForceAppPointDistance(this WheelCollider wheelCollider, float to, float duration)
    {
        return STween.Play<ActionFloatObject<WheelCollider>, WheelCollider, float>(wheelCollider, wheelCollider.forceAppPointDistance, to, duration, _changeWheelColliderForceAppPointDistance);
    }
    public static STweenState twnForceAppPointDistance(this WheelCollider wheelCollider, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<WheelCollider>, WheelCollider, float>(wheelCollider, from, to, duration, _changeWheelColliderForceAppPointDistance);
    }


    private static readonly Action<WheelCollider, float> _changeWheelColliderMass = __changeWheelColliderMass;
    private static void __changeWheelColliderMass(WheelCollider wheelCollider, float val) { wheelCollider.mass = val; }
    public static STweenState twnMass(this WheelCollider wheelCollider, float to, float duration)
    {
        return STween.Play<ActionFloatObject<WheelCollider>, WheelCollider, float>(wheelCollider, wheelCollider.mass, to, duration, _changeWheelColliderMass);
    }
    public static STweenState twnMass(this WheelCollider wheelCollider, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<WheelCollider>, WheelCollider, float>(wheelCollider, from, to, duration, _changeWheelColliderMass);
    }


    private static readonly Action<WheelCollider, float> _changeWheelColliderMotorTorque = __changeWheelColliderMotorTorque;
    private static void __changeWheelColliderMotorTorque(WheelCollider wheelCollider, float val) { wheelCollider.motorTorque = val; }
    public static STweenState twnMotorTorque(this WheelCollider wheelCollider, float to, float duration)
    {
        return STween.Play<ActionFloatObject<WheelCollider>, WheelCollider, float>(wheelCollider, wheelCollider.motorTorque, to, duration, _changeWheelColliderMotorTorque);
    }
    public static STweenState twnMotorTorque(this WheelCollider wheelCollider, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<WheelCollider>, WheelCollider, float>(wheelCollider, from, to, duration, _changeWheelColliderMotorTorque);
    }


    private static readonly Action<WheelCollider, float> _changeWheelColliderRadius = __changeWheelColliderRadius;
    private static void __changeWheelColliderRadius(WheelCollider wheelCollider, float val) { wheelCollider.radius = val; }
    public static STweenState twnRadius(this WheelCollider wheelCollider, float to, float duration)
    {
        return STween.Play<ActionFloatObject<WheelCollider>, WheelCollider, float>(wheelCollider, wheelCollider.radius, to, duration, _changeWheelColliderRadius);
    }
    public static STweenState twnRadius(this WheelCollider wheelCollider, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<WheelCollider>, WheelCollider, float>(wheelCollider, from, to, duration, _changeWheelColliderRadius);
    }


    private static readonly Action<WheelCollider, float> _changeWheelColliderSteerAngle = __changeWheelColliderSteerAngle;
    private static void __changeWheelColliderSteerAngle(WheelCollider wheelCollider, float val) { wheelCollider.steerAngle = val; }
    public static STweenState twnSteerAngle(this WheelCollider wheelCollider, float to, float duration)
    {
        return STween.Play<ActionFloatObject<WheelCollider>, WheelCollider, float>(wheelCollider, wheelCollider.steerAngle, to, duration, _changeWheelColliderSteerAngle);
    }
    public static STweenState twnSteerAngle(this WheelCollider wheelCollider, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<WheelCollider>, WheelCollider, float>(wheelCollider, from, to, duration, _changeWheelColliderSteerAngle);
    }


    private static readonly Action<WheelCollider, float> _changeWheelColliderSuspensionDistance = __changeWheelColliderSuspensionDistance;
    private static void __changeWheelColliderSuspensionDistance(WheelCollider wheelCollider, float val) { wheelCollider.suspensionDistance = val; }
    public static STweenState twnSuspensionDistance(this WheelCollider wheelCollider, float to, float duration)
    {
        return STween.Play<ActionFloatObject<WheelCollider>, WheelCollider, float>(wheelCollider, wheelCollider.suspensionDistance, to, duration, _changeWheelColliderSuspensionDistance);
    }
    public static STweenState twnSuspensionDistance(this WheelCollider wheelCollider, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<WheelCollider>, WheelCollider, float>(wheelCollider, from, to, duration, _changeWheelColliderSuspensionDistance);
    }


    private static readonly Action<WheelCollider, float> _changeWheelColliderWheelDampingRate = __changeWheelColliderWheelDampingRate;
    private static void __changeWheelColliderWheelDampingRate(WheelCollider wheelCollider, float val) { wheelCollider.wheelDampingRate = val; }
    public static STweenState twnWheelDampingRate(this WheelCollider wheelCollider, float to, float duration)
    {
        return STween.Play<ActionFloatObject<WheelCollider>, WheelCollider, float>(wheelCollider, wheelCollider.wheelDampingRate, to, duration, _changeWheelColliderWheelDampingRate);
    }
    public static STweenState twnWheelDampingRate(this WheelCollider wheelCollider, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<WheelCollider>, WheelCollider, float>(wheelCollider, from, to, duration, _changeWheelColliderWheelDampingRate);
    }


    private static readonly Action<WheelCollider, Vector3> _changeWheelColliderCenter = __changeWheelColliderCenter;
    private static void __changeWheelColliderCenter(WheelCollider wheelCollider, Vector3 val) { wheelCollider.center = val; }
    public static STweenState twnCenter(this WheelCollider wheelCollider, Vector3 to, float duration)
    {
        return STween.Play<ActionVector3Object<WheelCollider>, WheelCollider, Vector3>(wheelCollider, wheelCollider.center, to, duration, _changeWheelColliderCenter);
    }
    public static STweenState twnCenter(this WheelCollider wheelCollider, Vector3 from, Vector3 to, float duration)
    {
        return STween.Play<ActionVector3Object<WheelCollider>, WheelCollider, Vector3>(wheelCollider, from, to, duration, _changeWheelColliderCenter);
    }




}