﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FTF
{
    public class RoomInfoDisplay : MonoBehaviour
    {
        public Text uiUserCounter;
        public GameObject uiRoomMaster;
        public string format;

        int beforeUserCount;

        private void Update()
        {
            if(Networker.State >= NetworkState.Room)
            {
                RoomInfo room = RoomInfo.main;

                if(uiUserCounter != null && room.currentUserCount != beforeUserCount)
                {
                    beforeUserCount = room.currentUserCount;
                    //uiUserCounter.text = string.Format(format, beforeUserCount, room.maxUser);
                    uiUserCounter.text = beforeUserCount.ToString() + " / " + room.maxUser.ToString();
                }

                if (uiRoomMaster != null && PlayerInstance.lookMain.IsRoomMaster != uiRoomMaster.activeSelf)
                {
                    uiRoomMaster.SetActive(PlayerInstance.lookMain.IsRoomMaster);
                }
            }
        }
    }
}