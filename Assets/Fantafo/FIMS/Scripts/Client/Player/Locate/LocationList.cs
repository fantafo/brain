﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// UserLocation의 목록을 갖고있는 컴포넌트다.
    /// </summary>
    public class LocationList : SMonoBehaviour
    {
        public bool absoluteLocationOnAwake;
        public UserLocation[] users;

        private void OnEnable()
        {
            if(absoluteLocationOnAwake)
            {
                UserLocationManager.main.absoluteLocation = this;
            }
        }
        private void OnDisable()
        {
            if(absoluteLocationOnAwake && UserLocationManager.main.absoluteLocation == this)
            {
                UserLocationManager.main.absoluteLocation = null;
            }
        }

        [Button]
        public void Collect()
        {
            users = GetComponentsInChildren<UserLocation>();
        }
    }
}