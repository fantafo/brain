﻿using System;

namespace FTF.Packet
{
    class C_Brain
    {
        public static byte[] Index(short index)
        {
            SLog.Debug("C_Brain Index", index);
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Brain_Index);
            w.WriteH(index);
            return w.ToBytes();
        }
    }
}