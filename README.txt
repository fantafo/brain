 *작업 진행
자연사 연구소에서 받은 프로젝트에 FIMS 시스템 추가
=>Earth, Safety 프로젝트에서 FIMS구조를 추출해 작업 진행.

 *Safety의 FIMS구조와 다른 내용
1. BuildFTF에 Pico 추가..
2. PicoSDK(Pvr_UnitySDK) 추가..
3. 불필요한 내용 제거
=>Safety에도 Pico 추가 예정..

 *작업이 필요한 부분
1. 수업 진행 중, 여러 이유로 한개의 기기가 재시작 될경우 해당 기기만 처음부터 시작되는 문제->네트워크 동기화 작업 필요
2. Pico 컨트롤러 미적용 -> 지구 프로젝트 적용 필요시 가져다가 사용

작성일 2019.01.18